# php-extended/php-ldap-filter-object

A library that implements the php-ldap-filter-interface library

![coverage](https://gitlab.com/php-extended/php-ldap-filter-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-ldap-filter-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-ldap-filter-object ^8`


## Basic Usage

This library creates dn with the following constructor :

```php

use PhpExtended\Ldap\LdapFilterNodeInterface;
use PhpExtended\Ldap\LdapFilterNodeValueInterface;
use PhpExtended\Ldap\LdapFilterNodeMulti;
use PhpExtended\Ldap\LdapFilterNodeNot;
use PhpExtended\Ldap\LdapFilterNodeValue;

$filter = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
	new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
	new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
	new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz'),
]);

// gets "(|(!(foo~=bar))(cn=foobar)(uid=barbaz))"

```

To parse ldap filters, do :

```

use PhpExtended\Ldap\LdapFilterParser;

$parser = new LdapFilterParser();
$ldn = $parser->parseString('(&(objectClass=Person)(sn=John)(cn=Snow))');

// $ldn instanceof LdapFilterNodeMultiInterface AND node with
// three children which are each one of the nodes

```


## License

MIT (See [license file](LICENSE)).
