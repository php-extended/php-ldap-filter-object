<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use PhpExtended\Parser\AbstractParser;

/**
 * LdapFilterParser class file.
 * 
 * This class is a simple implementation of the LdapFilterParserInterface.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\AbstractParser<LdapFilterNodeMultiInterface>
 */
class LdapFilterParser extends AbstractParser implements LdapFilterParserInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterParserInterface::parseString()
	 */
	public function parse(?string $data) : LdapFilterNodeMultiInterface
	{
		if(null === $data || '' === $data)
		{
			return new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND);
		}
		
		$lpar = \mb_strpos($data, '(');
		if(false === $lpar)
		{
			return new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND);
		}
		
		$data = (string) \mb_substr($data, $lpar);
		
		$nodes = [];
		
		do
		{
			$token = $this->parseFilterNode($data);
			$node = $token->getNode();
			if(null !== $node)
			{
				$nodes[] = $node;
			}
			$data = $token->getLdapString();
		}
		while(!empty($data));
		
		if(empty($nodes))
		{
			return new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND);
		}
		
		if(1 === \count($nodes))
		{
			$node = $nodes[0];
			if(!\is_subclass_of($node, LdapFilterNodeMultiInterface::class))
			{
				return new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [$node]);
			}
			
			return $node;
		}
		
		return new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, $nodes);
	}
	
	/**
	 * Parses a given node.
	 * 
	 * @param string $ldapFilter
	 * @return LdapFilterNodeToken
	 */
	public function parseFilterNode(string $ldapFilter) : LdapFilterNodeToken
	{
		if(0 === \mb_strlen($ldapFilter))
		{
			return new LdapFilterNodeToken(null, '');
		}
		
		$opr = $ldapFilter[0];
		if('(' === $opr)
		{
			$ldapFilter = (string) \mb_substr($ldapFilter, 1);
			if(0 === \mb_strlen($ldapFilter))
			{
				return new LdapFilterNodeToken(null, '');
			}
			
			$opr = $ldapFilter[0];
		}
		
		switch($opr)
		{
			case LdapFilterNodeInterface::OP_AND:
			case LdapFilterNodeInterface::OP_OR:
				$ldapFilter = (string) \mb_substr($ldapFilter, 1);
				
				return $this->parseMultiNode($opr, $ldapFilter);
				
			case LdapFilterNodeInterface::OP_NOT:
				$ldapFilter = (string) \mb_substr($ldapFilter, 1);
				
				return $this->parseNotNode($ldapFilter);
		}
		
		return $this->parseValueNode($ldapFilter);
	}
	
	/**
	 * Parses a multi node.
	 * 
	 * @param string $opr
	 * @param string $ldapFilter
	 * @return LdapFilterNodeToken
	 */
	public function parseMultiNode(string $opr, string $ldapFilter) : LdapFilterNodeToken
	{
		$lpos = \mb_strpos($ldapFilter, '(');
		if(false === $lpos)
		{
			return new LdapFilterNodeToken(null, '');
		}
		
		$ldapFilter = (string) \mb_substr($ldapFilter, $lpos);
		$nodes = [];

		do
		{
			$token = $this->parseFilterNode($ldapFilter);
			$node = $token->getNode();
			if(null !== $node)
			{
				$nodes[] = $node;
			}
			
			$ldapFilter = $token->getLdapString();
			if(1 < \mb_strlen($ldapFilter))
			{
				$next = $ldapFilter[0];
				if(')' === $next)
				{
					$ldapFilter = (string) \mb_substr($ldapFilter, 1);
					break;
				}
			}
		}
		while(!empty($ldapFilter));
		
		if(0 === \count($nodes))
		{
			return new LdapFilterNodeToken(null, $ldapFilter);
		}
		
		if(1 === \count($nodes))
		{
			return new LdapFilterNodeToken($nodes[0], $ldapFilter);
		}
		
		return new LdapFilterNodeToken(new LdapFilterNodeMulti($opr, $nodes), $ldapFilter);
	}
	
	/**
	 * Parses a not node.
	 * 
	 * @param string $ldapFilter
	 * @return LdapFilterNodeToken
	 */
	public function parseNotNode(string $ldapFilter) : LdapFilterNodeToken
	{
		$lpos = \mb_strpos($ldapFilter, '(');
		if(false === $lpos)
		{
			$rpos = \mb_strpos($ldapFilter, ')');
			if(false !== $rpos)
			{
				return new LdapFilterNodeToken(null, (string) \mb_substr($ldapFilter, $rpos + 1));
			}
			
			return new LdapFilterNodeToken(null, '');
		}
		
		$ldapFilter = (string) \mb_substr($ldapFilter, $lpos);
		$token = $this->parseFilterNode($ldapFilter);
		$node = $token->getNode();
		if(null !== $node)
		{
			$ldapFilter = '';
			$rpos = \mb_strpos($token->getLdapString(), ')');
			if(false !== $rpos)
			{
				$ldapFilter = (string) \mb_substr($token->getLdapString(), $rpos + 1);
			}
			
			return new LdapFilterNodeToken(new LdapFilterNodeNot($node), $ldapFilter);
		}
		
		return new LdapFilterNodeToken(null, $token->getLdapString());
	}
	
	/**
	 * Parses a node value.
	 * 
	 * @param string $ldapFilter
	 * @return LdapFilterNodeToken
	 */
	public function parseValueNode(string $ldapFilter) : LdapFilterNodeToken
	{
		// default case is column=value filter
		$rpos = \mb_strpos($ldapFilter, ')');
		if(false === $rpos)
		{
			$rpos = (int) \mb_strlen($ldapFilter);
		}
		
		$comparison = (string) \mb_substr($ldapFilter, 0, $rpos);
		$newLdapFilter = '';
		if(\mb_strlen($ldapFilter) > $rpos)
		{
			$newLdapFilter = (string) \mb_substr($ldapFilter, $rpos + 1);
		}
		
		$lpar = \mb_strpos($comparison, '(');
		if(false !== $lpar)
		{
			$comparison = (string) \mb_substr($comparison, $lpar + 1);
		}
		
		if(0 === \mb_strlen($comparison))
		{
			return new LdapFilterNodeToken(null, $newLdapFilter);
		}
		
		$eqpos = \mb_strpos($comparison, '=');
		if(false !== $eqpos)
		{
			$column = (string) \mb_substr($comparison, 0, $eqpos);
			$value = (string) \mb_substr($comparison, $eqpos + 1);
			
			return $this->parseColumnValue($column, $value, $newLdapFilter);
		}
		
		$comparison = \trim($comparison);
		if(0 === \mb_strlen($comparison))
		{
			return new LdapFilterNodeToken(null, $newLdapFilter);
		}
		
		return new LdapFilterNodeToken(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, $this->ldapUnescape($comparison), ''), $newLdapFilter);
	}
	
	/**
	 * Parses the column and values.
	 * 
	 * @param string $column
	 * @param string $value
	 * @param string $ldapFilter
	 * @return LdapFilterNodeToken
	 */
	public function parseColumnValue(string $column, string $value, string $ldapFilter) : LdapFilterNodeToken
	{
		if(0 === \mb_strlen($column))
		{
			return new LdapFilterNodeToken(null, $ldapFilter);
		}
		
		$prev = $column[(int) \mb_strlen($column) - 1];
		
		switch($prev)
		{
			case '~':
			case '>':
			case '<':
				$fullop = $prev.'=';
				$column = (string) \mb_substr($column, 0, -1);
				
				$column = \trim($column);
				if(0 === \mb_strlen($column))
				{
					return new LdapFilterNodeToken(null, $ldapFilter);
				}
				
				return new LdapFilterNodeToken(new LdapFilterNodeValue($fullop, $column, $value), $ldapFilter);
		}
		
		$column = \trim($column);
		if(0 === \mb_strlen($column))
		{
			return new LdapFilterNodeToken(null, $ldapFilter);
		}
		
		return new LdapFilterNodeToken(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, $this->ldapUnescape($column), $this->ldapUnescape($value)), $ldapFilter);
	}
	
	/**
	 * Replaces all escape sequences in ldap values by their right characters.
	 *
	 * @param string $string
	 * @return string
	 */
	public function ldapUnescape(string $string) : string
	{
		$newString = $string;
		
		while(false !== \mb_strpos($newString, '\\'))
		{
			$unescaped = \str_replace([
				'\\00', '\\26', '\\28', '\\29', '\\2a', '\\2f', '\\3c', '\\3d', '\\3e', '\\5c', '\\7c', '\\7e',
			], [
				'\\0', '&', '(', ')', '*', '/', '<', '=', '>', '\\', '|', '~',
			], $newString);
			
			// prevent infinite loop with \ characters
			if($unescaped === $newString)
			{
				break;
			}
			
			$newString = $unescaped;
		}
		
		return $newString;
	}
	
}
