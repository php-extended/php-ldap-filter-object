<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

/**
 * LdapFilterNodeValue class file.
 * 
 * This class is a simple implementation of the LdapFilterNodeValueInterface.
 * 
 * @author Anastaszor
 */
class LdapFilterNodeValue implements LdapFilterNodeValueInterface
{
	
	/**
	 * The column.
	 * 
	 * @var string
	 */
	protected string $_column;
	
	/**
	 * The comparator.
	 * 
	 * @var string
	 */
	protected string $_comparator;
	
	/**
	 * The value.
	 * 
	 * @var string
	 */
	protected string $_value = '';
	
	/**
	 * Builds a new LdapFilterNodeValue with the given comparator, column and
	 * value.
	 * 
	 * @param string $comparator
	 * @param string $column
	 * @param string $value
	 */
	public function __construct(string $comparator, string $column, string $value)
	{
		switch($comparator)
		{
			case LdapFilterNodeValueInterface::CMP_APPROX:
			case LdapFilterNodeValueInterface::CMP_EQUALS:
			case LdapFilterNodeValueInterface::CMP_GREATER:
			case LdapFilterNodeValueInterface::CMP_LOWER:
				$this->_comparator = $comparator;
				break;
			
			default:
				$this->_comparator = LdapFilterNodeValueInterface::CMP_EQUALS;
				break;
		}
		$this->_column = $column;
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getStringRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return empty($this->_column);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeValueInterface::getColumn()
	 */
	public function getColumn() : string
	{
		return $this->_column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeValueInterface::getComparator()
	 */
	public function getComparator() : string
	{
		return $this->_comparator;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeValueInterface::getValue()
	 */
	public function getValue() : string
	{
		return $this->_value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeInterface::getStringRepresentation()
	 */
	public function getStringRepresentation() : string
	{
		if(empty($this->_column))
		{
			return '';
		}
		
		return '('.$this->ldapsecure($this->_column).$this->_comparator.$this->ldapvalue($this->_value).')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeInterface::getOperator()
	 */
	public function getOperator() : string
	{
		return LdapFilterNodeInterface::OP_AND;
	}
	
	/**
	 * Replaces all special chacaters used in ldap queries by allowed characters
	 * in order to prevent ldap injection.
	 *
	 * @param string $string
	 * @return string
	 */
	public function ldapsecure(string $string) : string
	{
		return \str_replace([
			'\\0', '&', '(', ')', '*', '/', '<', '=', '>', '\\', '|', '~',
		], [
			'\\00', '\\26', '\\28', '\\29', '\\2a', '\\2f', '\\3c', '\\3d', '\\3e', '\\5c', '\\7c', '\\7e',
		], $string);
	}
	
	/**
	 * Replaces all special chacaters used in ldap queries by allowed characters
	 * in order to prevent ldap injection.
	 *
	 * @param string $string
	 * @return string
	 */
	public function ldapvalue(string $string) : string
	{
		// https://tools.ietf.org/html/rfc2254
		// we do not replace *, which are joker caracters, user allowed
		// we do not replace /, which are used as real characters in some OUs
		// we do not replace =, which are used in the dn's
		return \str_replace([
			'\\0', '&', '(', ')', /* '*',    '/', */ '<', /* '=', */ '>', '\\', '|', '~',
		], [
			'\\00', '\\26', '\\28', '\\29', /* '\\2a', '\\2f', */ '\\3c', /* '\\3d', */ '\\3e', '\\5c', '\\7c', '\\7e',
		], $string);
	}
	
}
