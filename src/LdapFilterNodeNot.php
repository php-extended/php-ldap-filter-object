<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

/**
 * LdapFilterNodeNot class file.
 * 
 * This class represents a logical not for filter node logic.
 * 
 * @author Anastaszor
 */
class LdapFilterNodeNot implements LdapFilterNodeSingleInterface
{
	
	/**
	 * The child node.
	 * 
	 * @var LdapFilterNodeInterface
	 */
	protected LdapFilterNodeInterface $_child;
	
	/**
	 * Builds a new LdapFilterNodeNot with the given children node.
	 * 
	 * @param LdapFilterNodeInterface $child
	 */
	public function __construct(LdapFilterNodeInterface $child)
	{
		$this->_child = $child;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getStringRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return $this->_child->isEmpty();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeSingleInterface::getChildNode()
	 */
	public function getChildNode() : LdapFilterNodeInterface
	{
		return $this->_child;
	}
	
	/**
	 * Gets a rfc compliant string representation of this filter.
	 *
	 * @return string
	 */
	public function getStringRepresentation() : string
	{
		$childstr = $this->_child->getStringRepresentation();
		if(empty($childstr))
		{
			return '';
		}
		
		return '('.$this->getOperator().$childstr.')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeInterface::getOperator()
	 */
	public function getOperator() : string
	{
		return LdapFilterNodeInterface::OP_NOT;
	}
	
}
