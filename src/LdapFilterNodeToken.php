<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapFilterNodeToken class file.
 *
 * This class represents a single node to build an ldap filter.
 *
 * @author Anastaszor
 */
class LdapFilterNodeToken implements Stringable
{
	
	/**
	 * @var ?LdapFilterNodeInterface
	 */
	protected ?LdapFilterNodeInterface $_node;
	
	/**
	 * @var string
	 */
	protected string $_ldapstr;
	
	/**
	 * Builds a new LdapFilterNodeToken with the given node and rest to parse.
	 * 
	 * @param ?LdapFilterNodeInterface $node
	 * @param string $ldapstr
	 */
	public function __construct(?LdapFilterNodeInterface $node, string $ldapstr)
	{
		$this->_node = $node;
		$this->_ldapstr = $ldapstr;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getLdapString();
	}
	
	/**
	 * Gets the current parsed node.
	 * 
	 * @return ?LdapFilterNodeInterface
	 */
	public function getNode() : ?LdapFilterNodeInterface
	{
		return $this->_node;
	}
	
	/**
	 * Gets the rest to parse.
	 * 
	 * @return string
	 */
	public function getLdapString() : string
	{
		return $this->_ldapstr;
	}
	
}
