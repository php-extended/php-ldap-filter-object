<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use ArrayIterator;
use Iterator;

/**
 * LdapFilterNodeMulti class file.
 * 
 * This class represents a logical OR or logical AND node.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class LdapFilterNodeMulti implements LdapFilterNodeMultiInterface
{
	
	/**
	 * The operator.
	 * 
	 * @var string
	 */
	protected string $_operator = LdapFilterNodeInterface::OP_AND;
	
	/**
	 * The children.
	 * 
	 * @var array<integer, LdapFilterNodeInterface>
	 */
	protected array $_children = [];
	
	/**
	 * Builds a new LdapFilterNodeMulti with the given operator and children.
	 * 
	 * @param string $operator
	 * @param array<integer, LdapFilterNodeInterface> $children
	 */
	public function __construct(string $operator, array $children = [])
	{
		switch($operator)
		{
			case LdapFilterNodeInterface::OP_OR:
				$this->_operator = $operator;
				break;
				
			default:
				$this->_operator = LdapFilterNodeInterface::OP_AND;
				break;
		}
		
		$this->addNodes($children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getStringRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function current() : LdapFilterNodeInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return \current($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		\next($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return (int) \key($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return false !== \current($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		\reset($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeInterface::getStringRepresentation()
	 */
	public function getStringRepresentation() : string
	{
		$representations = [];
		
		foreach($this as $key => $child)
		{
			$representation = $child->getStringRepresentation();
			if(!empty($representation))
			{
				$representations[$key] = $representation;
			}
		}
		
		if(1 >= \count($representations))
		{
			return \implode('', $representations);
		}
		
		return '('.$this->getOperator().\implode('', $representations).')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeInterface::getOperator()
	 */
	public function getOperator() : string
	{
		return $this->_operator;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return 0 === \count($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addNode()
	 */
	public function addNode(LdapFilterNodeInterface $node) : LdapFilterNodeMultiInterface
	{
		if($node->isEmpty())
		{
			return $this;
		}
		
		$this->_children[] = $node;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addNodes()
	 */
	public function addNodes(array $nodes) : LdapFilterNodeMultiInterface
	{
		return $this->addNodeIterator(new ArrayIterator($nodes));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addNodeIterator()
	 */
	public function addNodeIterator(Iterator $nodes) : LdapFilterNodeMultiInterface
	{
		foreach($nodes as $node)
		{
			$this->addNode($node);
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addValue()
	 */
	public function addValue(string $column, $value, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface
	{
		return $this->addNode(new LdapFilterNodeValue($comparator, $column, (string) $value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addNotValue()
	 */
	public function addNotValue(string $column, $value, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface
	{
		return $this->addNode(new LdapFilterNodeNot(new LdapFilterNodeValue($comparator, $column, (string) $value)));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addAndValues()
	 */
	public function addAndValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface
	{
		return $this->addAndIterator($column, new ArrayIterator($values), $comparator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addAndIterator()
	 */
	public function addAndIterator(string $column, Iterator $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface
	{
		if(LdapFilterNodeInterface::OP_AND === $this->getOperator())
		{
			foreach($values as $value)
			{
				$this->addValue($column, $value, $comparator);
			}
			
			return $this;
		}
		
		$andNode = new self(LdapFilterNodeInterface::OP_AND);
		$andNode->addAndIterator($column, $values, $comparator);
		
		return $this->addNode($andNode);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addNandValues()
	 */
	public function addNandValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface
	{
		return $this->addNandIterator($column, new ArrayIterator($values), $comparator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addNandIterator()
	 */
	public function addNandIterator(string $column, Iterator $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface
	{
		$andNode = new self(LdapFilterNodeInterface::OP_AND);
		$andNode->addAndIterator($column, $values, $comparator);
		
		return $this->addNode(new LdapFilterNodeNot($andNode));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addOrValues()
	 */
	public function addOrValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface
	{
		return $this->addOrIterator($column, new ArrayIterator($values), $comparator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addOrIterator()
	 */
	public function addOrIterator(string $column, Iterator $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface
	{
		if(LdapFilterNodeInterface::OP_OR === $this->getOperator())
		{
			foreach($values as $value)
			{
				$this->addValue($column, $value, $comparator);
			}
			
			return $this;
		}
		
		$orNode = new self(LdapFilterNodeInterface::OP_OR);
		$orNode->addOrIterator($column, $values, $comparator);
		
		return $this->addNode($orNode);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addNorValues()
	 */
	public function addNorValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface
	{
		return $this->addNorIterator($column, new ArrayIterator($values), $comparator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addNorIterator()
	 */
	public function addNorIterator(string $column, Iterator $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface
	{
		$orNode = new self(LdapFilterNodeInterface::OP_OR);
		$orNode->addOrIterator($column, $values, $comparator);
		
		return $this->addNode(new LdapFilterNodeNot($orNode));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addXorValues()
	 */
	public function addXorValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface
	{
		return $this->addXorIterator($column, new ArrayIterator($values), $comparator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::addXorIterator()
	 */
	public function addXorIterator(string $column, Iterator $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapFilterNodeMultiInterface
	{
		$currentNode = $this;
		
		foreach($values as $value)
		{
			$valueNode = new LdapFilterNodeValue($comparator, $column, (string) $value);
			
			$and1Node = new self(LdapFilterNodeInterface::OP_AND);
			$and1Node->addNode($currentNode);
			$and1Node->addNode(new LdapFilterNodeNot($valueNode));
			
			$and2Node = new self(LdapFilterNodeInterface::OP_AND);
			$and2Node->addNode(new LdapFilterNodeNot($currentNode));
			$and2Node->addNode($valueNode);
			
			$orNode = new self(LdapFilterNodeInterface::OP_OR);
			$orNode->addNode($and1Node);
			$orNode->addNode($and2Node);
			
			$currentNode = $orNode;
		}
		
		return $currentNode;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapFilterNodeMultiInterface::removeNodesFor()
	 */
	public function removeNodesFor(string $column) : LdapFilterNodeMultiInterface
	{
		$ret = $this->getFilteredNode($this, $column);
		if($ret instanceof LdapFilterNodeMultiInterface)
		{
			return $ret;
		}
		
		return new self($this->_operator);
	}
	
	/**
	 * Gets the node if it is not filtered by the column, null if it is.
	 * 
	 * @param LdapFilterNodeInterface $node
	 * @param string $column
	 * @return ?LdapFilterNodeInterface
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function getFilteredNode(LdapFilterNodeInterface $node, string $column) : ?LdapFilterNodeInterface
	{
		if($node instanceof LdapFilterNodeMultiInterface)
		{
			$children = [];
			
			/** @var LdapFilterNodeInterface $child */
			foreach($node as $child)
			{
				$filtered = $this->getFilteredNode($child, $column);
				if(null !== $filtered)
				{
					$children[] = $filtered;
				}
			}
			
			if(!empty($children))
			{
				$class = \get_class($node);
				
				return new $class($node->getOperator(), $children);
			}
		}
		
		if($node instanceof LdapFilterNodeSingleInterface)
		{
			$result = $this->getFilteredNode($node->getChildNode(), $column);
			if(null !== $result)
			{
				$class = \get_class($node);
				
				return new $class($result);
			}
		}
		
		if($node instanceof LdapFilterNodeValueInterface)
		{
			if($node->getColumn() !== $column)
			{
				return $node;
			}
		}
		
		return null;
	}
	
}
