<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapFilterNodeInterface;
use PhpExtended\Ldap\LdapFilterNodeMulti;
use PhpExtended\Ldap\LdapFilterNodeNot;
use PhpExtended\Ldap\LdapFilterNodeValue;
use PhpExtended\Ldap\LdapFilterNodeValueInterface;
use PHPUnit\Framework\TestCase;

/**
 * LdapFilterNodeMultiTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapFilterNodeMulti
 *
 * @internal
 *
 * @small
 */
class LdapFilterNodeMultiTest extends TestCase
{
	
	/**
	 * @var LdapFilterNodeMulti
	 */
	protected LdapFilterNodeMulti $_filter;
	
	public function testOperator() : void
	{
		$this->assertEquals(LdapFilterNodeInterface::OP_OR, $this->_filter->getOperator());
	}
	
	public function testOperatorEmpty() : void
	{
		$filter = new LdapFilterNodeMulti('');
		$this->assertEquals(LdapFilterNodeInterface::OP_AND, $filter->getOperator());
	}
	
	public function testCount() : void
	{
		$this->assertEquals(2, $this->_filter->count());
	}
	
	public function testEmpty() : void
	{
		$this->assertTrue((new LdapFilterNodeMulti(''))->isEmpty());
	}
	
	public function testIsEmpty() : void
	{
		$this->assertFalse($this->_filter->isEmpty());
	}
	
	public function testIsEmpty2() : void
	{
		$node = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, '', 'toto'),
		]);
		$this->assertTrue($node->isEmpty());
	}
	
	public function testToStringEmpty() : void
	{
		$filter = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR);
		$this->assertEquals('', $filter->__toString());
	}
	
	public function testToStringOne() : void
	{
		$filter = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
		]);
		$this->assertEquals('(cn=foobar)', $filter->__toString());
	}
	
	public function testToString() : void
	{
		$this->assertEquals('(|(!(foo~=bar))(cn=foobar))', $this->_filter->__toString());
	}
	
	public function testAddNode() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz'),
		]);
		$this->assertEquals($expected, $this->_filter->addNode($node));
	}
	
	public function testAddNodes() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz'),
		]);
		$this->assertEquals($expected, $this->_filter->addNodes([$node]));
	}
	
	public function testAddNodeIterator() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz'),
		]);
		$this->assertEquals($expected, $this->_filter->addNodeIterator(new ArrayIterator([$node])));
	}
	
	public function testAddNodeValue() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz'),
		]);
		$this->assertEquals($expected, $this->_filter->addValue('uid', 'barbaz', LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddNotNodeValue() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz')),
		]);
		$this->assertEquals($expected, $this->_filter->addNotValue('uid', 'barbaz', LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddAndValues() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz')]),
		]);
		$this->assertEquals($expected, $this->_filter->addAndValues('uid', ['barbaz'], LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddAndIterator() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz')]),
		]);
		$this->assertEquals($expected, $this->_filter->addAndIterator('uid', new ArrayIterator(['barbaz']), LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddNandValues() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			new LdapFilterNodeNot(new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz')])),
		]);
		$this->assertEquals($expected, $this->_filter->addNandValues('uid', ['barbaz'], LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddNandIterator() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			new LdapFilterNodeNot(new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz')])),
		]);
		$this->assertEquals($expected, $this->_filter->addNandIterator('uid', new ArrayIterator(['barbaz']), LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddOrValues() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz'),
		]);
		$this->assertEquals($expected, $this->_filter->addOrValues('uid', ['barbaz'], LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddOrMultiValues() : void
	{
		$this->_filter = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
		]);
		
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz')]),
		]);
		$this->assertEquals($expected, $this->_filter->addOrValues('uid', ['barbaz'], LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddOrMultiIterator() : void
	{
		$this->_filter = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
		]);
		
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz')]),
		]);
		$this->assertEquals($expected, $this->_filter->addOrIterator('uid', new ArrayIterator(['barbaz']), LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddOrIterator() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz'),
		]);
		$this->assertEquals($expected, $this->_filter->addOrIterator('uid', new ArrayIterator(['barbaz']), LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddNorValues() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			new LdapFilterNodeNot(new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz')])),
		]);
		$this->assertEquals($expected, $this->_filter->addNorValues('uid', ['barbaz'], LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddNorIterator() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
			new LdapFilterNodeNot(new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz')])),
		]);
		$this->assertEquals($expected, $this->_filter->addNorIterator('uid', new ArrayIterator(['barbaz']), LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddXorValues() : void
	{
		$old = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
		]);
		
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [
				$old,
				new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz')),
			]),
			new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [
				new LdapFilterNodeNot($old),
				new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz'),
			]),
		]);
		
		$this->assertEquals($expected, $this->_filter->addXorValues('uid', ['barbaz'], LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testAddXorIterator() : void
	{
		$old = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
		]);
		
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [
				$old,
				new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz')),
			]),
			new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [
				new LdapFilterNodeNot($old),
				new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'barbaz'),
			]),
		]);
		
		$this->assertEquals($expected, $this->_filter->addXorIterator('uid', new ArrayIterator(['barbaz']), LdapFilterNodeValueInterface::CMP_EQUALS));
	}
	
	public function testRemoveNodeFor() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
		]);
		$this->assertEquals($expected, $this->_filter->removeNodesFor('cn'));
	}
	
	public function testRemoveNodeMulti() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
		]);
		$this->assertEquals($expected, $this->_filter->addNode(
			new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_AND, [new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'barfoo')]),
		)->removeNodesFor('cn'));
	}
	
	public function testRemoveAll() : void
	{
		$this->assertEquals(new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR), $this->_filter->removeNodesFor('foo')->removeNodesFor('cn'));
	}
	
	protected function setUp() : void
	{
		$this->_filter = new LdapFilterNodeMulti(LdapFilterNodeInterface::OP_OR, [
			new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'foo', 'bar')),
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'foobar'),
		]);
	}
	
}
