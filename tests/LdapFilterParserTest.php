<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapFilterNodeMulti;
use PhpExtended\Ldap\LdapFilterNodeMultiInterface;
use PhpExtended\Ldap\LdapFilterNodeNot;
use PhpExtended\Ldap\LdapFilterNodeToken;
use PhpExtended\Ldap\LdapFilterNodeValue;
use PhpExtended\Ldap\LdapFilterNodeValueInterface;
use PhpExtended\Ldap\LdapFilterParser;
use PHPUnit\Framework\TestCase;

/**
 * LdapFilterParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapFilterParser
 *
 * @internal
 *
 * @small
 */
class LdapFilterParserTest extends TestCase
{
	
	/**
	 * @var LdapFilterParser
	 */
	protected LdapFilterParser $_parser;
	
	public function testToString() : void
	{
		$object = $this->_parser;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testParseStringEmpty() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$this->assertEquals($expected, $this->_parser->parse(null));
	}
	
	public function testParseStringVoid() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$this->assertEquals($expected, $this->_parser->parse('abcdefgh'));
	}
	
	public function testParseStringNonexistant() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$this->assertEquals($expected, $this->_parser->parse('  (  )  '));
	}
	
	public function testParseStringValue() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$expected->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'));
		$this->assertEquals($expected, $this->_parser->parse(' (foo=bar)'));
	}
	
	public function testParseDoubleValue() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeMulti::OP_AND);
		$expected->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'));
		$expected->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'toto'));
		$this->assertEquals($expected, $this->_parser->parse('(&(foo=bar)(cn=toto))'));
	}
	
	public function testParseDoubleValue2() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeMulti::OP_OR);
		$expected->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'));
		$expected->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'toto'));
		$this->assertEquals($expected, $this->_parser->parse('(|(foo=bar)(cn=toto))'));
	}
	
	public function testParseConjugatedValues() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeMulti::OP_AND);
		$expected->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'));
		$expected->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'toto'));
		$this->assertEquals($expected, $this->_parser->parse('(foo=bar)(cn=toto)'));
	}
	
	public function testParseFilterNodeEmpty() : void
	{
		$expected = new LdapFilterNodeToken(null, '');
		$this->assertEquals($expected, $this->_parser->parseFilterNode(''));
	}
	
	public function testParseFilterNodeEmpty2() : void
	{
		$expected = new LdapFilterNodeToken(null, '');
		$this->assertEquals($expected, $this->_parser->parseFilterNode('('));
	}
	
	public function testParseFilterNodeOr() : void
	{
		$node = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_OR);
		$node->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'toto'));
		$node->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'tata'));
		$expected = new LdapFilterNodeToken($node, '');
		$this->assertEquals($expected, $this->_parser->parseFilterNode('|    (uid=toto)    (cn=tata))'));
	}
	
	public function testParseFilterNodeAnd() : void
	{
		$node = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$node->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'toto'));
		$node->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'tata'));
		$expected = new LdapFilterNodeToken($node, '');
		$this->assertEquals($expected, $this->_parser->parseFilterNode('&    (uid=toto)    (cn=tata))'));
	}
	
	public function testParseFilterNot() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'toto');
		$node = new LdapFilterNodeNot($node);
		$expected = new LdapFilterNodeToken($node, '');
		$this->assertEquals($expected, $this->_parser->parseFilterNode('!   (uid=toto))'));
	}
	
	public function testParseMultiNodeEmpty() : void
	{
		$expected = new LdapFilterNodeToken(null, '');
		$this->assertEquals($expected, $this->_parser->parseMultiNode('&', '))'));
	}
	
	public function testParseMultiNodeEmpty2() : void
	{
		$expected = new LdapFilterNodeToken(null, '');
		$this->assertEquals($expected, $this->_parser->parseMultiNode('&', '())'));
	}
	
	public function testParseMultiNodeOne() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeMultiInterface::OP_AND, 'uid', 'toto');
		$expected = new LdapFilterNodeToken($node, '');
		$this->assertEquals($expected, $this->_parser->parseMultiNode('&', '(uid=toto))'));
	}
	
	public function testParseMultiNode() : void
	{
		$node = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$node->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'toto'));
		$node->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'tata'));
		$expected = new LdapFilterNodeToken($node, '');
		$this->assertEquals($expected, $this->_parser->parseMultiNode('&', '(uid=toto)(cn=tata))'));
	}
	
	public function testParseMultiNodeRemainder() : void
	{
		$node = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_OR);
		$node->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'toto'));
		$node->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'tata'));
		$expected = new LdapFilterNodeToken($node, '(foo=bar)');
		$this->assertEquals($expected, $this->_parser->parseMultiNode('|', '(uid=toto)(cn=tata))(foo=bar)'));
	}
	
	public function testParseNotNodeNoBegin() : void
	{
		$expected = new LdapFilterNodeToken(null, '');
		$this->assertEquals($expected, $this->_parser->parseNotNode(' '));
	}
	
	public function testParseNotNodeWithEnd() : void
	{
		$expected = new LdapFilterNodeToken(null, 'cn=tata)');
		$this->assertEquals($expected, $this->_parser->parseNotNode(' )cn=tata)'));
	}
	
	public function testParseNotNodeNotClosed() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'toto');
		$node = new LdapFilterNodeNot($node);
		$expected = new LdapFilterNodeToken($node, '');
		$this->assertEquals($expected, $this->_parser->parseNotNode(' (uid=toto'));
	}
	
	public function testParseNotNode() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'toto');
		$node = new LdapFilterNodeNot($node);
		$expected = new LdapFilterNodeToken($node, '');
		$this->assertEquals($expected, $this->_parser->parseNotNode(' (uid=toto))'));
	}
	
	public function testParseNotNodeEmpty() : void
	{
		$expected = new LdapFilterNodeToken(null, '(cn=toto)');
		$this->assertEquals($expected, $this->_parser->parseNotNode('(=tata)(cn=toto)'));
	}
	
	public function testParseValueEquals() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'toto');
		$expected = new LdapFilterNodeToken($node, '');
		$this->assertEquals($expected, $this->_parser->parseValueNode('uid=toto'));
	}
	
	public function testParseValueApprox() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_APPROX, 'uid', 'toto');
		$expected = new LdapFilterNodeToken($node, '');
		$this->assertEquals($expected, $this->_parser->parseValueNode('uid~=toto'));
	}
	
	public function testParseValueGreater() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_GREATER, 'uid', 'toto');
		$expected = new LdapFilterNodeToken($node, '');
		$this->assertEquals($expected, $this->_parser->parseValueNode('uid>=toto'));
	}
	
	public function testParseValueLower() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_LOWER, 'uid', 'toto');
		$expected = new LdapFilterNodeToken($node, '');
		$this->assertEquals($expected, $this->_parser->parseValueNode('uid<=toto'));
	}
	
	public function testParseValueEmpty() : void
	{
		$expected = new LdapFilterNodeToken(null, '');
		$this->assertEquals($expected, $this->_parser->parseValueNode('(  >=abc)'));
	}
	
	public function testParseValueEmpty2() : void
	{
		$expected = new LdapFilterNodeToken(null, '');
		$this->assertEquals($expected, $this->_parser->parseValueNode('(  = abc)'));
	}
	
	public function testParseValueRemainder() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'toto');
		$expected = new LdapFilterNodeToken($node, '(cn=tata)');
		$this->assertEquals($expected, $this->_parser->parseValueNode('uid=toto)(cn=tata)'));
	}
	
	public function testEmptyComparison() : void
	{
		$expected = new LdapFilterNodeToken(null, '(cn=tata)');
		$this->assertEquals($expected, $this->_parser->parseValueNode(')(cn=tata)'));
	}
	
	public function testNoComparison() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'abcd', '');
		$expected = new LdapFilterNodeToken($node, '(cn=tata)');
		$this->assertEquals($expected, $this->_parser->parseValueNode('abcd)(cn=tata)'));
	}
	
	public function testEmptyColumn() : void
	{
		$expected = new LdapFilterNodeToken(null, '(cn=tata)');
		$this->assertEquals($expected, $this->_parser->parseValueNode('=toto)(cn=tata)'));
	}
	
	public function testEscapedColumn() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'to\\to');
		$expected = new LdapFilterNodeToken($node, '');
		$this->assertEquals($expected, $this->_parser->parseValueNode('(cn=to\\5c5cto)'));
	}
	
	public function testParserRfc01() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$expected->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'Babs Jensen'));
		$this->assertEquals($expected, $this->_parser->parse('(cn=Babs Jensen)'));
	}
	
	public function testParseRfc02() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$expected->addNode(new LdapFilterNodeNot(
			new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'Tim Howes'),
		));
		$this->assertEquals($expected, $this->_parser->parse('(!(cn=Tim Howes))'));
	}
	
	public function testParseRfc03() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$expected->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'objectClass', 'Person'));
		$or = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_OR);
		$or->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'sn', 'Jensen'));
		$or->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'Babs J*'));
		$expected->addNode($or);
		$this->assertEquals($expected, $this->_parser->parse('(&(objectClass=Person)(|(sn=Jensen)(cn=Babs J*)))'));
	}
	
	public function testParseRfc04() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$expected->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'o', 'univ*of*mich*'));
		$this->assertEquals($expected, $this->_parser->parse('(o=univ*of*mich*)'));
	}
	
	public function testParseRfc05() : void
	{
		$expected = new LdapFilterNodeMulti(LdapFilterNodeMultiInterface::OP_AND);
		$expected->addNode(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'seeAlso', ''));
		$this->assertEquals($expected, $this->_parser->parse('(seeAlso=)'));
	}
	
	protected function setUp() : void
	{
		$this->_parser = new LdapFilterParser();
	}
	
}
