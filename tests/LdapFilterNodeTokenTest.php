<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapFilterNodeInterface;
use PhpExtended\Ldap\LdapFilterNodeToken;
use PhpExtended\Ldap\LdapFilterNodeValue;
use PhpExtended\Ldap\LdapFilterNodeValueInterface;
use PHPUnit\Framework\TestCase;

/**
 * LdapFilterNodeTokenTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapFilterNodeToken
 *
 * @internal
 *
 * @small
 */
class LdapFilterNodeTokenTest extends TestCase
{
	
	/**
	 * @var LdapFilterNodeInterface
	 */
	protected LdapFilterNodeInterface $_node;
	
	/**
	 * @var LdapFilterNodeToken
	 */
	protected LdapFilterNodeToken $_token;
	
	public function testToString() : void
	{
		$this->assertEquals('toto', $this->_token->__toString());
	}
	
	public function testSameNode() : void
	{
		$this->assertEquals($this->_node, $this->_token->getNode());
	}
	
	public function testSameString() : void
	{
		$this->assertEquals('toto', $this->_token->getLdapString());
	}
	
	protected function setUp() : void
	{
		$this->_node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'cn', 'toto');
		$this->_token = new LdapFilterNodeToken($this->_node, 'toto');
	}
	
}
