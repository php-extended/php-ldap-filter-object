<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapFilterNodeInterface;
use PhpExtended\Ldap\LdapFilterNodeNot;
use PhpExtended\Ldap\LdapFilterNodeValue;
use PhpExtended\Ldap\LdapFilterNodeValueInterface;
use PHPUnit\Framework\TestCase;

/**
 * LdapFilterNodeNotTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapFilterNodeNot
 *
 * @internal
 *
 * @small
 */
class LdapFilterNodeNotTest extends TestCase
{
	
	/**
	 * @var LdapFilterNodeNot
	 */
	protected LdapFilterNodeNot $_filter;
	
	public function testChildNode() : void
	{
		$this->assertEquals(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'), $this->_filter->getChildNode());
	}
	
	public function testOperator() : void
	{
		$this->assertEquals(LdapFilterNodeInterface::OP_NOT, $this->_filter->getOperator());
	}
	
	public function testIsEmpty() : void
	{
		$node = new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, '', 'toto'));
		$this->assertTrue($node->isEmpty());
	}
	
	public function testToString() : void
	{
		$this->assertEquals('(!(foo=bar))', $this->_filter->__toString());
	}
	
	public function testToStringEmpty() : void
	{
		$node = new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, '', 'toto'));
		$this->assertEquals('', $node->__toString());
	}
	
	protected function setUp() : void
	{
		$this->_filter = new LdapFilterNodeNot(new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'foo', 'bar'));
	}
	
}
