<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-filter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapFilterNodeInterface;
use PhpExtended\Ldap\LdapFilterNodeValue;
use PhpExtended\Ldap\LdapFilterNodeValueInterface;
use PHPUnit\Framework\TestCase;

/**
 * LdapFilterNodeValueTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapFilterNodeValue
 *
 * @internal
 *
 * @small
 */
class LdapFilterNodeValueTest extends TestCase
{
	
	/**
	 * @var LdapFilterNodeValue
	 */
	protected LdapFilterNodeValue $_filter;
	
	public function testOperator() : void
	{
		$this->assertEquals(LdapFilterNodeInterface::OP_AND, $this->_filter->getOperator());
	}
	
	public function testComparator() : void
	{
		$this->assertEquals(LdapFilterNodeValueInterface::CMP_EQUALS, $this->_filter->getComparator());
	}
	
	public function testComparatorLower() : void
	{
		$comparator = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_LOWER, 'uid', 'foo');
		$this->assertEquals(LdapFilterNodeValueInterface::CMP_LOWER, $comparator->getComparator());
	}
	
	public function testComparatorEmpty() : void
	{
		$comparator = new LdapFilterNodeValue('', 'uid', 'foo');
		$this->assertEquals(LdapFilterNodeValueInterface::CMP_EQUALS, $comparator->getComparator());
	}
	
	public function testColumn() : void
	{
		$this->assertEquals('uid', $this->_filter->getColumn());
	}
	
	public function testValue() : void
	{
		$this->assertEquals('foo', $this->_filter->getValue());
	}
	
	public function testIsEmpty() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, '', 'toto');
		$this->assertTrue($node->isEmpty());
	}
	
	public function testStringEmpty() : void
	{
		$node = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, '', 'toto');
		$this->assertEquals('', $node->__toString());
	}
	
	public function testToString() : void
	{
		$this->assertEquals('(uid=foo)', $this->_filter->__toString());
	}
	
	protected function setUp() : void
	{
		$this->_filter = new LdapFilterNodeValue(LdapFilterNodeValueInterface::CMP_EQUALS, 'uid', 'foo');
	}
	
}
